FROM node:18.15-alpine as dependencies
WORKDIR /outfit-creator-web
COPY package.json package-lock.json ./
RUN npm ci

FROM node:18.15-alpine as builder
WORKDIR /outfit-creator-web
COPY . .
COPY --from=dependencies /outfit-creator-web/node_modules ./node_modules
RUN npm run build

FROM node:18.15-alpine as runner
WORKDIR /outfit-creator-web
ENV NODE_ENV production

COPY --from=builder /outfit-creator-web/public ./public
COPY --from=builder /outfit-creator-web/package.json ./package.json
COPY --from=builder /outfit-creator-web/.next ./.next
COPY --from=builder /outfit-creator-web/node_modules ./node_modules

EXPOSE 3000
CMD ["npm", "start"]
